const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const QuestionSchema = mongoose.Schema({
    quiz: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Quiz'
    },
    sequence_number: {
        type: Number,
        required: true
    },
    question_data: {
        type: mongoose.Schema.Types.ObjectId,
        refPath: 'question_model',
        required: true
    },
    question_model: {
        type: String,
        required: true,
        enum: ['YesNoQuestion', 'MultipleChoiceQuestion', 'DragDropQuestion']
    },
    time_limit: {
        type: Number,
        default: 0
    }
});

QuestionSchema.index({quiz: 1, sequence_number: 1}, {unique: true});

QuestionSchema.pre('remove', async function(next) {
    await this.question_data.deleteOne();
    next();
});

const Question = module.exports = mongoose.model('Question', QuestionSchema);
