const GameRoom = require('../models/gameroom');
const events = require('./eventNames');
const {decode} = require('html-entities');
const {isUserAlreadyInGame, emitToRoom, prepareChatMessage, errorFunction, justSuccessFunction, dataFunction} = require('./socketUtils');
const grStorage = require('./gameRoomsStorage');
const {shuffleArray, arrayEquals} = require('../util/arrayHelper');
const {getOpenTriviaAuthor} = require('../util/openTrivia');
const Quiz = require('../models/quiz')


const fetchGameRoomDocAndStorage = async (gameroom_id) => {
    const doc   = await GameRoom.findById(gameroom_id).populate('participants');
    const store = await grStorage.getGameRoom(gameroom_id);

    return {doc, store};   
}


const prepareQuestion = (question) => {
    switch(question.question_model) {
        case 'YesNoQuestion':
            break;
        case 'MultipleChoiceQuestion':
            shuffleArray(question.question_data.options);
            break;
        case 'DragDropQuestion':
            question.question_data.items = question.question_data.items.map((item, index) => {return {text: item, index}});
            shuffleArray(question.question_data.items);
            break;
        default:
            console.log('Unknown question type!');
            break;
    }
}

const prepareQuestionForUser = (question) => {
    switch (question.question_model){
        case 'YesNoQuestion':
            return {
                question_id: question._id.toString(),
                question_data: {
                    text: decode(question.question_data.text),
                },
                question_model: question.question_model,
                time_limit: question.time_limit
            }
        case 'MultipleChoiceQuestion':
            return {
                question_id: question._id.toString(),
                question_model: question.question_model,
                question_data: {
                    text: decode(question.question_data.text),
                    multiple_correct: question.question_data.multiple_correct,
                    options: question.question_data.options.map(qdb => {
                        return decode(qdb.option_text);
                    })
                },
                time_limit: question.time_limit
            }
        case 'DragDropQuestion':
            return {
                question_id: question._id.toString(),
                question_model: question.question_model,
                question_data: {
                    text: decode(question.question_data.text),
                    items: question.question_data.items.map(item => {
                        return decode(item.text);
                    })
                },
                time_limit: question.time_limit
            }
        default:
            console.log('Unknown question type!');
            return null;
    }
}

const isCorrect = (question, answer) => {
    switch (question.question_model){
        case 'YesNoQuestion':
            if (question.question_data.answer === answer){
                return {correct: true, message: 'Correct!'};
            }else{
                return {correct: false, correct_answer: question.question_data.answer, message: 'Incorrect!'};
            }
            break;
        case 'MultipleChoiceQuestion':
            // NOTE: You do not send the answer string, you send the INDEX (0 based) of the option
            if (question.question_data.multiple_correct){
                let correct_answer = question.question_data.options.map(option => {return option.correct});
                if (answer!==null && answer!==undefined && arrayEquals(answer, correct_answer)) {
                    return {correct: true, message: 'Correct!'};
                } else {
                    return {correct: false, correct_answer: correct_answer, message: 'Incorrect!'};
                }
            } else {
                // answer and response are indexes, not strings
                if (answer!==null && answer!==undefined && answer>=0 && answer<question.question_data.options.length && question.question_data.options[answer].correct) {
                    return {correct: true, message: 'Correct!'};
                } else {
                    let correct_answer = question.question_data.options.map(opt => {return opt.correct}).indexOf(true);
                    return {correct: false, correct_answer: correct_answer, message: 'Incorrect!'};
                }
            }
            break;
        case 'DragDropQuestion':
            let correct_answer = question.question_data.items.map(item => {return item.index});
            if (answer!==null && answer!==undefined && arrayEquals(answer, correct_answer)) {
                return {correct: true, message: 'Correct!'};
            } else {
                return {correct: false, correct_answer: correct_answer, message: 'Incorrect!'};
            }
            break;
        default:
            console.log(`INVALID (or new?) QUESTION MODEL: ${question.question_model}`);
            return {correct: false, correct_answer: `Invalid question model (did you add a new question?)`, message: 'Eh'};
    }
}

const killGameRoom = async (doc, store, gameroom_id) => {
    try{
        await GameRoom.deleteOne({_id: gameroom_id});
    }catch(err){
        console.log(`[user_left] Error deleting gameroom document (possibly triggered multiple times by multiple users leaving, not necessarily an error.)`);
        console.log(`the error is: `)
        console.log(err);
    }

    if (store.hasOwnProperty('current_question_timeout')){
        clearTimeout(store.current_question_timeout);
    }
    if (store.hasOwnProperty('cooldown_timeout')){
        clearTimeout(store.cooldown_timeout);
    }
    if (store.hasOwnProperty('kill_timeout')){
        clearTimeout(store.kill_timeout);
    }

    if (store.hasOwnProperty('last_question_chill_timeout')){
        clearTimeout(store.last_question_chill_timeout);
    }

    let quiz_id = store.quiz._id.toString();
    let author_id = store.quiz.author._id.toString();
    let opent_author_id = (await getOpenTriviaAuthor()).toString()

    if (!grStorage.deleteGameRoom(gameroom_id)){
        console.log(`GameRoom not found in storage map (possibly triggered multiple times by multiple users leaving, not necessarily an error.)`);
    }
    if (author_id == opent_author_id){
        
        console.log(`Deleting quiz (opentrivia).`);
        console.log(await Quiz.deleteOne({_id: quiz_id}));
        // TODO: Cascade

    }
}

const onEveryoneLeft = async (gameroom_id) => {
    // Assumes the event already handled storage and doc removal
    const {doc, store} = await fetchGameRoomDocAndStorage(gameroom_id);
    if (store.participants.length === 0){ // This is already being checked but cannot hurt
        // The game is now considered aborted
        await killGameRoom(doc, store, gameroom_id);

    }else{
        console.log('Everyone left emitted but participants still exist, possibly an error.');
    }
}

module.exports.onUserLeft = async (socket, flag) => {
    const {doc, store} = await fetchGameRoomDocAndStorage(socket.current_game_id);
    if (!doc || !store){
        return;
    }
        
    if (!doc.participants.some(p => p._id.toString() === socket.user.id)){
        socket.emit(events.GAMEROOM_QUIZ_LEAVE, `You're not in this gameroom.`);
        return;
    }

    if (!store.participants.some(p => p.id.toString() === socket.user.id)){
        socket.emit(events.GAMEROOM_QUIZ_LEAVE, `You're not in this gameroom.`);
        return;            
    }

    const user_id = socket.user.id;
    const gameroom_id = socket.current_game_id;

    doc.participants = doc.participants.filter(x => {return x._id.toString() != user_id});
    store.participants = store.participants.filter(x => {return x.id.toString() != user_id});
    await doc.save();
    
    socket.to(gameroom_id).emit(events.GAMEROOM_QUIZ_CHAT_MSG_META_RECV, {gameroom_id: gameroom_id,
                                    chat_message: `User ${socket.user.username} has just left!`});
                                                                              
    socket.to(gameroom_id).emit(events.GAMEROOM_QUIZ_LEAVE, {gameroom_id: gameroom_id,
        user: {username: socket.user.username},
        chat_message: `User ${socket.user.username} has just left!`});
    socket.leave(gameroom_id);
    socket.emit(events.GAMEROOM_QUIZ_LEAVE, {message: `You've successfully left the gameroom.`});

    if (store.participants.length == 0){
        console.log(`Everyone left, emitting.`)
        await onEveryoneLeft(socket.current_game_id);
    }else{
        console.log(`There are still ${store.participants.length} users in the game, so it goes on.`)
    }
}

module.exports.registerGameRoomEvents = (socket, io) => {

    const chillAndBroadcast = async (io, gameroom_id) => {
        const tag = "[Gr events: Chill and broadcast]"
        const store = grStorage.getGameRoom(gameroom_id);
        if (store.state != grStorage.GAMEROOM_STATE.IDLE 
            && store.state != grStorage.GAMEROOM_STATE.AWAITING_ANSWERS){
                console.log(`INVALID STATE IN CHILL&BROADCAST!`);
                console.log(`\t -- state: ${store.state}`);
                return;
        }
        store.state = grStorage.GAMEROOM_STATE.COOLDOWN;
        store.cooldown_timeout = setTimeout(async () => {
            if (store.state != grStorage.GAMEROOM_STATE.COOLDOWN){
                console.log(`INVALID STATE IN BROADCAST CALLBACK!`);
                console.log(`\t -- state: ${store.state}`);
                return;
            }
            const question_object = {
                ...store.quiz.questions[store.current_question_idx]
            }

            store.num_answers_to_current = 0;
            store.state = grStorage.GAMEROOM_STATE.AWAITING_ANSWERS;

            prepareQuestion(question_object);
            io.in(gameroom_id).emit(events.GAMEROOM_QUIZ_NEXT_QUESTION, prepareQuestionForUser(question_object));
            store.current_question_timeout = setTimeout(() => {
                onQuestionDone(io, gameroom_id, 1)
            }, question_object.time_limit*1000);
            store.current_question_time_started = new Date().getTime();
            
        }, 1000); // Cooldown period (hardcoded for now)
    }

    const onQuestionDone = async(io, gameroom_id, flag) => {
        if (flag == 1){
            console.log(`Time exceeded.`);
        }else{
            console.log(`Everyone answered.`);
        }

        const {doc, store} = await fetchGameRoomDocAndStorage(gameroom_id);

        const answered_question_id = store.current_question_id;
        const answered_question_idx = store.current_question_idx;
        const answered_question_object = store.quiz.questions[answered_question_idx];
        const sockets_in_room = io.sockets.adapter.rooms.get(gameroom_id)
        console.log(`Emitting to all sockets in room ${sockets_in_room}`)
        try{
            for (const socket_id of sockets_in_room)
                {
                    const socket_obj = io.sockets.sockets.get(socket_id);
                    const user_id = socket_obj.user.id;    
                    const users_answer = store.user_answers[user_id][answered_question_id];
                    const feedback = isCorrect(answered_question_object, users_answer);
                    const response = {
                        message: `Question is done! now chill for a bit...`,
                        your_answer: users_answer,
                        question_id: answered_question_id,
                        feedback,
                        gameroom_id
                    };
                    if (!feedback.correct){
                        store.user_results[user_id][answered_question_id] = 0;
                    }
                    
                    socket_obj.emit(events.GAMEROOM_QUIZ_QUESTION_FEEDBACK, response);
                }
        }catch(err){
            // NOTE: This means that all users left the game
            // (the sockets_in_room becomes non-iterable for some reason)
            console.log('All users left in the meantime, destroy gameroom!');
            console.log(`Also, sockets in room is ${sockets_in_room}`)
            console.log(err);
            console.log(`Emitting everyone left`);
            await onEveryoneLeft(gameroom_id);
            return;
        }
        
        
        const new_question_index = ++store.current_question_idx;
        if (new_question_index == store.quiz.questions.length){
            store.last_question_chill_timeout = setTimeout(async () => {
                if (!store || !doc){
                    return;
                }
                try{
                    // for (const socket_id of sockets_in_room)
                    // {
                    //     const socket_obj = io.sockets.sockets.get(socket_id);  
                    //     const final_feedback = {feedback: "Something :D"};
                    //     const response = {
                    //         message: `The quiz has finished.`,
                    //         final_feedback,
                    //         gameroom_id
                    //     };
                    //     // socket_obj.emit(events.GAMEROOM_QUIZ_QUESTION_FEEDBACK, response);
                    // }
                    
                    let rankings = []
                    for (const participant of store.participants){
                        const username = participant.username;
                        const photo = participant.photo;
                        const id = participant.id.toString();
                        let total_score = store.user_results[id].reduce((x, y) => {return x + y;});
                        rankings.push({username, photo, total_score});
                    }
    
                    rankings.sort((obj1, obj2) => {
                        const p1 = obj1.total_score;
                        const p2 = obj2.total_score;
                        return p2 - p1;
                    });
    
                    rankings = rankings.map(entry => {return {...entry, total_score: Math.floor(entry.total_score*100)}});
    
                    io.in(gameroom_id).emit(events.GAMEROOM_QUIZ_FINISHED, {
                        message: `The quiz has finished. The gameroom will get destroyed soon, feel free to insult each other in the meantime.`,
                        rankings,
                        gameroom_id
                    });
                    store.state = grStorage.GAMEROOM_STATE.DONE;
                    store.kill_timeout = setTimeout(async () => {
                        // So the client knows when to leave
                        io.in(gameroom_id).emit(events.GAMEROOM_QUIZ_DESTROYED, {});
                        await killGameRoom(doc, store, gameroom_id);
                    }, 5000)
                }catch(err){
                    console.log(`emitting el, err:`)
                    console.log(err);
                    await onEveryoneLeft(gameroom_id);
                }
            }, 1000);
            
        }else{
            store.num_answers_to_current = 0;
            store.current_question_id = store.quiz.questions[store.current_question_idx]._id.toString();
            await chillAndBroadcast(io, gameroom_id);
        }

    
    }

    socket.on(events.GAMEROOM_QUIZ_JOIN, async (data) => {
        const sendError = errorFunction(socket, events.GAMEROOM_QUIZ_JOIN);
        const sendSuccess = justSuccessFunction(socket, events.GAMEROOM_QUIZ_JOIN);
        const sendData = dataFunction(socket, events.GAMEROOM_QUIZ_JOIN);

        const user_id = socket.user.id;
        const gameroom_id = data.gameroom_id;
        const game_code = data.game_code;
        const pin = data.pin;
        
        const {doc, store} = await fetchGameRoomDocAndStorage(gameroom_id);

        if (!doc || !store)
        {
            sendError('Game not found.');
            return;
        }
        
        if (doc.participants.some(p => p._id.toString() === socket.user.id)){
            sendError(`You're already in this gameroom.`);
            return;
        }

        if (store.participants.some(p => p.id.toString() === socket.user.id)){
            sendError(`You're already in this gameroom.`);
            return;            
        }

        if (store.state != grStorage.GAMEROOM_STATE.IDLE)
        {
            sendError(`You cannot join a game that's already started.`);
            return;
        }

        if (!store.multiplayer && socket.user.id.toString() != store.author._id.toString())
        {
            sendError(`This is not your game.`);
            return;
        }

        // Author doesn't need to provide a PIN (no problem if he does, it just gets ignored)
        if (store.locked && socket.user.id.toString() != store.author._id.toString() && pin != store.pin){
            sendError('Wrong PIN.');
            return;
        }
                
        doc.participants.push(socket.user.id);
        store.participants.push(socket.user);
        await doc.save();

        socket.join(gameroom_id);
        socket.to(gameroom_id).emit(events.GAMEROOM_QUIZ_CHAT_MSG_META_RECV, {gameroom_id: gameroom_id,
                                                                              user: {username: socket.user.username, photo: socket.user.photo},
                                                                             chat_message: `User ${socket.user.username} has just joined!`});
        

        // I tried to avoid this but this way it is much easier to
        // to handle user leaving mid-game
        // Otherwise we'd have to go through all games in storage and find the user                                                                                
        socket.current_game_id = gameroom_id;

        sendSuccess(`You've just joined gameroom: ${doc.gameroom_name}`);
        
    });

    socket.on(events.GAMEROOM_QUIZ_LEAVE, async (data) => {       
        await this.onUserLeft(socket, 1);
     
    });  

    socket.on(events.GAMEROOM_QUIZ_CHAT_MSG_SEND, async (data) => {
        const sendError = errorFunction(socket, events.GAMEROOM_QUIZ_CHAT_MSG_SEND);
        const sendSuccess = justSuccessFunction(socket, events.GAMEROOM_QUIZ_CHAT_MSG_SEND);

        const prepRes = prepareChatMessage(socket, data);
        if (!prepRes.success){
            console.log(`Emitting MSG_SENT error (prep)`);
            sendError(prepRes.message);
            return;
        }
        const res = emitToRoom(socket, io, prepRes.data, events.GAMEROOM_QUIZ_CHAT_MSG_RECV);
        if (res.success){
            sendSuccess('Message sent!');
        }else{
            console.log(`Emitting MSG_SENT error (room)`);
            sendError(res.message);
        }
    });

    // TODO: Validate later
    socket.on(events.GAMEROOM_QUIZ_START, async (data) => {
        const sendError = errorFunction(socket, events.GAMEROOM_QUIZ_START);
        const sendSuccess = justSuccessFunction(socket, events.GAMEROOM_QUIZ_START);
        
        const user_id = socket.user.id;
        const gameroom_id = data.gameroom_id;
        const store = await grStorage.getGameRoom(gameroom_id);

        if (!store){
            return;
        }

        if (!store.participants.some(p => p.id.toString() === socket.user.id)){
            sendError(`You're not in this gameroom.`);
            return;            
        }

        if (store.author._id.toString() != user_id)
        {
            sendError(`You don't have permission to start this game.`);
            return;
        }

        if (store.state != grStorage.GAMEROOM_STATE.IDLE){
            sendError(`This game has already been started!`);
            return;
        }
        
        store.current_question_idx = 0;        
        store.current_question_id = store.quiz.questions[store.current_question_idx]._id.toString();

        //user_id:
        // question_id: (correct, timeleft)

        store.user_answers = {}
        store.user_results = {}
        for (const participant of store.participants){
            store.user_answers[participant.id.toString()] = {}
            store.user_results[participant.id.toString()] = new Array(store.quiz.questions.length).fill(0);
            for (const question of store.quiz.questions){
                store.user_answers[participant.id.toString()][question._id.toString()] = null;
            }
        }
        io.in(gameroom_id).emit(events.GAMEROOM_QUIZ_CHAT_MSG_META_RECV, {gameroom_id: data.gameroom_id, chat_message: `The game will now start...`});
        sendSuccess(`You've successfully started the game.`);
        
        await chillAndBroadcast(io, gameroom_id);
    });

    socket.on(events.GAMEROOM_QUIZ_ANSWER_QUESTION, async (data) => {
        const sendError = errorFunction(socket, events.GAMEROOM_QUIZ_ANSWER_QUESTION);
        const sendSuccess = justSuccessFunction(socket, events.GAMEROOM_QUIZ_ANSWER_QUESTION);

        const user_id = socket.user.id;
        const gameroom_id = data.gameroom_id;
        const store = await grStorage.getGameRoom(gameroom_id);
        

        if (!store.participants.some(p => p.id.toString() === socket.user.id)){
            sendError(`You're not in this gameroom.`);
            return;            
        }

        if (!store.state == grStorage.GAMEROOM_STATE.AWAITING_ANSWERS){
            return {success: false, message: `You cannot answer a question right now.`}
        }

        const question_id = data.question_id;
        if (!store.current_question_id == question_id){
            sendError(`Invalid question ID (this is not the current question)`);
            return;
        }

        const user_answer = data.answer;
        store.user_answers[user_id][store.current_question_id] = user_answer;

        const current_question_object = store.quiz.questions[store.current_question_idx];
        const ta = new Date().getTime();
        const tl = current_question_object.time_limit;
        const t0 = store.current_question_time_started;

        store.user_results[user_id][store.current_question_idx] = (tl - (ta - t0)/1000)/tl;
        store.num_answers_to_current ++;

        if (store.num_answers_to_current == store.participants.length)
        {
            clearTimeout(store.current_question_timeout);
            onQuestionDone(io, gameroom_id, 2);
        }
        sendSuccess('Question answered!');

    });


  
    
}