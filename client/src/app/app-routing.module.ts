import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { DeactivateGuard } from './guards/deactivate.guard';
import { HomeComponent } from './layout/home/home.component';
import { ProfileComponent } from './layout/profile/profile.component';
import { LoginAuthGuard } from './guards/login-auth.guard';
import { LoginComponent } from './layout/login/login.component';
import { RegisterComponent } from './layout/register/register.component';
import { QuizComponent } from './layout/quiz/quiz.component';
import { GameRoomComponent } from './layout/game-room/game-room.component';
import { MakeOpenTComponent } from './layout/make-opent/make-opent.component';
import { GameroomDeactivateGuard } from './guards/gameroom-deactivate.guard';
import { EditQuizComponent } from './layout/edit-quiz/edit-quiz.component';
import { MakeQuizComponent } from './layout/make-quiz/make-quiz.component';
import { QuizDetailsComponent } from './layout/quiz-details/quiz-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch:'full' },
  { path: 'home', component:HomeComponent, canActivate:[AuthGuard] },
  { path: 'profile', component:ProfileComponent, canActivate:[AuthGuard] },
  { path: 'login', component: LoginComponent, canActivate: [LoginAuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [LoginAuthGuard] },
  { path: 'quiz/:id', component: QuizComponent, canActivate: [AuthGuard], canDeactivate: [DeactivateGuard] },
  { path: 'quiz/details/:quiz_id', component: QuizDetailsComponent, canActivate: [AuthGuard] },
  { path: 'gameroom/:gameroom-id', component: GameRoomComponent, canActivate: [AuthGuard], canDeactivate: [GameroomDeactivateGuard] },
  { path: 'makeOpenT', component: MakeOpenTComponent, canActivate: [AuthGuard]},
  { path: 'editQuiz/:quiz_id', component: EditQuizComponent, canActivate: [AuthGuard] },
  { path: 'makeQuiz', component: MakeQuizComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
