import { TestBed } from '@angular/core/testing';

import { GameroomDeactivateGuard } from './gameroom-deactivate.guard';

describe('GameroomDeactivateGuard', () => {
  let guard: GameroomDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(GameroomDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
