import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuizService } from 'src/app/services/quiz.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-quiz-details',
  templateUrl: './quiz-details.component.html',
  styleUrls: ['./quiz-details.component.scss', '../../layout.scss', '../../quiz.scss']
})
export class QuizDetailsComponent implements OnInit {

  quiz: any;

  dialogActive: boolean = false;

  dialogButtons: string[];
  dialogText: string;

  constructor(
    private authService: AuthenticationService,
    private uiService: UiService,
    private quizService: QuizService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.quiz_id;
    this.quizService.getQuizDetails(id).subscribe(data => {
      this.quiz = data;
    });
  }

  openDialog(): void {
    this.dialogButtons = ['Yes', 'No'];
    this.dialogText = 'Would you like to lock this room?';
    this.dialogActive = true;
  }

  getChoice(grLocked: string) {
    this.startMultiplayer(grLocked === 'Yes');
  }

  startSingleplayer(): void {
    this.router.navigate([`/quiz/${this.route.snapshot.params.quiz_id}`]);
  }

  startMultiplayer(isLocked: boolean): void {
    this.quizService.createGameroom(this.quiz._id, `${this.authService.user.username}'s room`, true, isLocked).subscribe(res => {
      this.router.navigate([`/gameroom/${res.gameroom_id}`]);
    });
  }

}
