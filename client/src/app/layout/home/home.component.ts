import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GameRoom } from 'src/app/models/gameRoom';
import { QuizService } from 'src/app/services/quiz.service';

import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss', '../../layout.scss']
})
export class HomeComponent implements OnInit {

  quizzes: Array<any> = new Array();
  categories: Array<any> = new Array();
  searched = false;
  search_results: any[] = new Array();

  opent_categories: any[] = new Array();
  latest_quizzes: any[] = new Array();
  gamerooms: GameRoom[];

  dialogActive: boolean = false;
  errorMessage: string = '';

  constructor(public authService: AuthenticationService, public quizService: QuizService, private router: Router) { }

  ngOnInit(): void {    
    this.quizService.getAllGamerooms().subscribe((res) => {
      this.gamerooms = res;
    })
    this.quizService.getCategories().subscribe(data => {
      this.opent_categories = data.categories;
    });
    this.quizService.getLatestQuizzes().subscribe(data => {
      this.latest_quizzes = data;
    });
    this.quizService.getQuizzes().subscribe(data => {
      this.quizzes = data.quizzes;
    });
  }

  findQuizzes(query: string): void {
    this.quizService.findQuizzes(query).subscribe(data => {
      this.searched = true;
      this.search_results = data;
    });
  }

  joinGameroom(inputText: string): void {

    let pin = 0;
    let gamecode = 0;

    if(inputText[1] != ""){
      pin = parseInt(inputText[1]);
    }
    
    gamecode = parseInt(inputText[0]);

    if (isNaN(pin)) {
      this.errorMessage = 'Gameroom PIN must be an integer!';
      return;
    }

    if (isNaN(gamecode)) {
      this.errorMessage = 'Gameroom code must be an integer!';
      return;
    }

    this.quizService.getPrivateGameRoomId(gamecode).subscribe(
      res => {
        this.router.navigate([`/gameroom/${res.id}`], { queryParams: { gamecode: gamecode, pin: pin } });
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

}
