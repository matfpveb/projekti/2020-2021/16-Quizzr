import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from '../../services/quiz.service';
@Component({
  selector: 'app-make-quiz',
  templateUrl: './make-quiz.component.html',
  styleUrls: ['./make-quiz.component.scss']
})
export class MakeQuizComponent implements OnInit {

  quizName: string = "";
  quizDescription: string = "";

  constructor(private quizService: QuizService, private router: Router) { }

  ngOnInit(): void {
  }

  submitQuiz($event: Event) {
    $event.stopPropagation();

    this.quizService.createQuiz(this.quizName, this.quizDescription).subscribe(res => {
      this.router.navigate([`editQuiz/${res.quiz._id}`]);
    }) ;
  }

}
