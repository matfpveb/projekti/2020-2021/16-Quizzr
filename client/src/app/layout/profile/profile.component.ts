import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, ValidatorFn } from '@angular/forms';

import { AuthenticationService } from '../../services/authentication.service'
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss', '../../layout.scss', '../../form.scss']
})
export class ProfileComponent implements OnInit {

  newPhotoFile: File;
  newPhoto: string;
  noPhoto: string = 'assets/default.jpg';

  photoError: boolean;
  photoMessage: string;

  passwordForm: FormGroup;

  passwordError: boolean;
  passwordMessage: string;

  constructor(
    public authService: AuthenticationService,
    public uiService: UiService,
    private formBuilder: FormBuilder
  ) {
    this.passwordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      newPasswordConfirm: ['', [Validators.required]]
    }, { validators: this.validateConfirmPassword });
  }

  ngOnInit(): void {
  }

  onPhotoSubmit(): void {
    this.authService.uploadPhoto(this.newPhotoFile).subscribe(data => {
      if (!data.success) {
        this.photoError = true;
      } else {
        this.photoError = false;
        this.authService.getUser();
      }
      this.photoMessage = data.message;
    });
  }

  onPhotoSelect(event: any): void {
    if (event.target.files && event.target.files[0]) {
      if(event.target.files[0].size >= 1000000){
        this.photoError = true;
        this.photoMessage = 'Image too large';
      } else {
        this.newPhotoFile = event.target.files[0];
        const reader = new FileReader();
        reader.onload = e => {
          let img = new Image();
          img.onload = () => {
            if(img.width == img.height) {
              this.newPhoto = reader.result.toString();
            } else {
              this.photoError = true;
              this.photoMessage = 'Image must have same height and width';
            }
          };
          img.src = reader.result.toString();
        };
        reader.readAsDataURL(this.newPhotoFile);
      }
    }
  }

  onPasswordSubmit(): void {
    const oldPassword: string = this.passwordForm.get('oldPassword').value;
    const newPassword: string = this.passwordForm.get('newPassword').value;
    
    this.authService.changePassword(oldPassword, newPassword).subscribe(data => {
        if (!data.success) {
          this.passwordError = true;
        } else {
          this.passwordError = false;
        }
        this.passwordMessage = data.message;
      });
  }

  validateConfirmPassword(group: FormGroup) {
    const newPassword: string = group.get('newPassword').value;
    const newPasswordConfirm: string = group.get('newPasswordConfirm').value;

    return newPassword !== newPasswordConfirm ? { match: true } : null;
  }

}
