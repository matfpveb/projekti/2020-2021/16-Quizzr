import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './layout/login/login.component';
import { HomeComponent } from './layout/home/home.component';
import { RegisterComponent } from './layout/register/register.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { ProfileComponent } from './layout/profile/profile.component';
import { MessageComponent } from './components/message/message.component';
import { QuizComponent } from './layout/quiz/quiz.component';
import { GameRoomComponent } from './layout/game-room/game-room.component';
import { MakeOpenTComponent } from './layout/make-opent/make-opent.component';
import { CountdownComponent } from './components/countdown/countdown.component';
import { EditQuizComponent } from './layout/edit-quiz/edit-quiz.component';
import { MakeQuizComponent } from './layout/make-quiz/make-quiz.component';
import { QuestionComponent } from './components/question/question.component';
import { QuizDetailsComponent } from './layout/quiz-details/quiz-details.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    NavbarComponent,
    ProfileComponent,
    MessageComponent,
    QuizComponent,
    GameRoomComponent,
    MakeOpenTComponent,
    CountdownComponent,
    EditQuizComponent,
    MakeQuizComponent,
    QuestionComponent,
    QuizDetailsComponent,
    SearchBoxComponent,
    DialogBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    DragDropModule,
    PickerModule,
    EmojiModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
