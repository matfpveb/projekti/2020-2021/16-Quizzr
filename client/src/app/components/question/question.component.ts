import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['../../form.scss', '../../layout.scss', './question.component.scss']
})
export class QuestionComponent implements OnInit, OnChanges {

  // TODO: test for drag and drop and multiple correct

  @Input() rawQuestion: any;
  @Input() feedback: any;

  currentQuestion: any = null;
  questionAnswered: boolean = false;
  questionNumber: number = 0;

  correctAnswer: number|boolean|Array<number>|Array<boolean> = null;

  @Output() answerQuestion: EventEmitter<any> = new EventEmitter();
  @Output() questionLoaded: EventEmitter<any> = new EventEmitter();

  myAnswer: number|boolean|Array<number>|Array<boolean> = null;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.feedback != undefined && changes.feedback.currentValue != null) {
      this.feedback = changes.feedback.currentValue;
      this.evaluateAnswer();
    }

    else if (changes.rawQuestion.currentValue != null) {
      this.rawQuestion = changes.rawQuestion.currentValue;
      this.prepareQuestion();
    }

  }

  initQuestion(question: any) : any {
    switch(question.question_model) {
      case 'YesNoQuestion':
        return question;
      case 'MultipleChoiceQuestion':
        if(question.question_data.multiple_correct)
          question.question_data.options = question.question_data.options.map(option => {return {text: option, checked: false}});
        return question;
      case 'DragDropQuestion':
        question.question_data.items = question.question_data.items.map((item, index) => {return {text: item, index}});
        return question;
      default:
        console.error('Unknown question type');
        return null;
    }
  }

  prepareQuestion(): void {
    this.questionAnswered = false;
    this.myAnswer = null;
    this.correctAnswer = null;
    
    this.questionNumber++;
    this.currentQuestion = this.initQuestion(this.rawQuestion);

    this.questionLoaded.emit(this.currentQuestion);
  }

  prepareAnswer(answer: number|boolean|number[]|boolean[]): void {
    if(this.questionAnswered)
      return;
    this.questionAnswered = true;
    this.myAnswer = answer;

    this.answerQuestion.emit(answer);
  }

  evaluateAnswer(): void {
    this.questionAnswered = true; // If question times out

    if(this.feedback.correct){
      this.correctAnswer = this.myAnswer;
    }else{
      this.correctAnswer = this.feedback.correct_answer;
    }
  }

  mapIndexes(items: any[]) : Array<number> {
    return items.map(item => item.index);
  }

  mapIndexesReverse(items: any[]) : Array<number> {
    const indexes = new Array(items.length);
    items.forEach((item, i) => {
      indexes[item.index] = i;
    });
    return indexes;
  }

  stripChecked(option: any) : Array<boolean> {
    return option.map(option => {return option.checked});
  }

  answerEquals(lhs: number|boolean|number[]|boolean[], rhs: number|boolean|number[]|boolean[]): boolean {
    if(typeof lhs !== typeof rhs) {
      return false;
    } else if(Array.isArray(lhs) && Array.isArray(rhs)) {
      return (lhs.length==rhs.length) && (lhs as any[]).every((x, i) => { return (rhs as any[])[i]===x });
    } else {
      return lhs === rhs;
    }
  }

  getOptionClass(option: number) : string {
    if(!this.questionAnswered || this.correctAnswer === null || this.myAnswer === null)
      return '';
    if((this.myAnswer as boolean[])[option] === (this.correctAnswer as boolean[])[option])
      return 'answer-correct';
    else
      return 'answer-incorrect';
  }

  getChoiceClass(choice: number|boolean): string {
    if(!this.questionAnswered)
      return '';
    else if(this.correctAnswer === null && this.answerEquals(this.myAnswer, choice))
      return 'answer-pending';
    if(this.answerEquals(this.myAnswer, this.correctAnswer)) {
      if(this.answerEquals(this.myAnswer, choice))
        return 'answer-correct';
      else
        return '';
    } else {
      if(this.answerEquals(this.myAnswer, choice))
        return 'answer-incorrect';
      else if(this.answerEquals(this.correctAnswer, choice))
        return 'answer-true';
      else
        return '';
    }
  }

  getAnswerClass(answer: number|boolean|number[]|boolean[]): string {
    if(!this.questionAnswered)
      return '';
    else if(this.correctAnswer === null)
      return 'answer-pending';
    if(this.answerEquals(this.myAnswer, this.correctAnswer)) {
      if(this.answerEquals(this.myAnswer, answer))
        return 'answer-correct';
      else
        return '';
    } else {
      if(this.answerEquals(this.myAnswer, answer) || (this.questionAnswered && this.myAnswer===null))
        return 'answer-incorrect';
      else
        return '';
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.currentQuestion.question_data.items, event.previousIndex, event.currentIndex);
  }
}
